# SmallProjects

## Name

Card Decks

## Description
This is the classic 'draw a card from a deck of cards' project.

The primary goals were a simple use of the random library and some very basic operator overloads.