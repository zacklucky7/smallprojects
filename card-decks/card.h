#ifndef CARD_H
#define CARD_H

#include <string>
#include <iostream>

class Card {
    std::string suit_;
    std::string value_;
    std::string info_;
    bool dealt_ = false;
public:
    Card(const char value, const char suit);
    Card(const std::string& value, const std::string& suit);

    const std::string& value() const {return value_;}
    const std::string& suit() const {return suit_;}
    std::string_view info() const {return info_;}
    const bool dealt() const {return dealt_;}
    void deal() {dealt_ = true;}

    bool operator== (const std::string& card_str);
    bool operator== (Card& card);
    bool operator!= (const std::string& card_str);
    bool operator!= (Card& card);

    friend std::ostream& operator<< (std::ostream& os, const Card& card);
};

#endif