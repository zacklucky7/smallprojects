#include <random>
#include "deck.h"

int main() {

    srand(time(nullptr));

    Deck d1;

    while (d1.remaining() > 0) {
        Card* card = d1.deal();
        if (card) {
            std::cout << "Drew a card: " << card->info() << "! Remaining: " << d1.remaining() << std::endl;
        } else {
            break;
        }
    }

    std::cout << "All done." << std::endl;

    return 0;

}