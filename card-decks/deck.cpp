#include <exception>
#include <random>
#include "deck.h"

Deck::Deck() {
    try {
        build_deck();
    } catch (const std::exception& e) {
        std::cout << "Could not assemble deck: " << e.what() << std::endl;
    }
}

Deck::~Deck() {
    if (cards_.size() > 0) {
        for (auto card: cards_) {
            card = nullptr;
            delete card;
        }
    }
}

void Deck::build_deck() {

    for (int suit_i = 0; suit_i < SUITS; suit_i++) {
        for (int value_i = 0; value_i < VALUES; value_i++) {
            add_card(suits_[suit_i], values_[value_i]);
        }
    }

}

int Deck::add_card(const char value, const char suit) {
    Card* card = new Card(value, suit);
    if (card) {
        cards_.push_back(card);
        return 0;
    } else {
        throw std::invalid_argument("A null card was added");
        return -1;
    }   
}

Card* Deck::deal() {

    while (true) {
        int card_i = rand() % MAX_CARDS;
        Card* card = cards_[card_i];
        if (!card->dealt()) {
            card->deal();
            remaining_--;
            return card;
        }
    }
    return nullptr;
    
}