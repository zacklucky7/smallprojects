#include "card.h"

Card::Card(const char suit, const char value):
    suit_{suit},
    value_{value},
    info_{value_ + suit_}
    {}

Card::Card(const std::string& suit, const std::string& value):
    suit_{suit},
    value_{value},
    info_{value_ + suit_}
    {}

bool Card::operator==(const std::string& card_str) {
    if (info().compare(card_str) == 0) {
        return true;
    }
    return false;
}

bool Card::operator==(Card& card) {
    if (info().compare(card.info()) == 0) {
        return true;
    }
    return false;
}

bool Card::operator!=(const std::string& card_str) {
    if (info().compare(card_str) != 0) {
        return true;
    }
    return false;
}

bool Card::operator!=(Card& card) {
    if (info().compare(card.info()) != 0) {
        return true;
    }
    return false;
}

std::ostream& operator<< (std::ostream& os, const Card& card) {
    os << card.info();
    return os;
}