#ifndef DECK_H
#define DECK_H

#include <vector>
#include "card.h"

class Deck {
    std::vector<Card*> cards_;
    static constexpr int SUITS = 4;
    static constexpr int VALUES = 13;
    const char suits_[SUITS] = {'H', 'D', 'C', 'S'};
    const char values_[VALUES] = {'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'};
    int remaining_ = SUITS * VALUES;
    static constexpr int MAX_CARDS = SUITS * VALUES;

    void build_deck();
    int add_card(const char value, const char suit);

public:

    Deck();
    ~Deck();
    Card* deal();
    int remaining() {return remaining_;}
};

#endif