#include "client.h"

void Client::swap(Command* newCommand) {
    activeCommand_ = newCommand;
}

void Client::execute() {
    if (activeCommand_ == nullptr) {
        puts("No command loaded in.");
    } else {
        activeCommand_->execute();
    }
}

void Client::addQueue() {
    if (activeCommand_ == nullptr) {
        puts("Could not queue empty command");
    } else {
        cmdQueue.push_back(activeCommand_);
    }
}

void Client::doQueue() {
    if (cmdQueue.size() > 0) {
        for (int i = 0; i < cmdQueue.size(); i++) {
            cmdQueue[i]->execute();
        }
    } else {
        puts("Cannot doQueue; queue is empty.");
    }
}

void Client::popQueue() {
    if (cmdQueue.size() > 0) {
        cmdQueue.pop_back();
    }
}