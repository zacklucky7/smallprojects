#ifndef COMMAND_H
#define COMMAND_H

#include <iostream>

class Command {
public:
    virtual void execute() = 0;
};

class HopCommand: public Command {
public:
    virtual void execute() { puts("Hop!"); }
};

class YeetCommand: public Command {
public:
    virtual void execute() { puts("Yeet!"); }
};

#endif