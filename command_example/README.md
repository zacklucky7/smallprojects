# SmallProjects

## Name

Command Example

## Description
This is a simple implementation of the Command design pattern.

Taken from main.cpp:

COMMAND PATTERN EXAMPLE

    This is an example implementation of the "command" code design pattern.

    "Encapsulate a request as an object, thereby letting users parameterize clients with different requests, queue or log requests, and support undoable operations."

    To qualify, several things must be accomplished:

    1. A "client" (this may be an object) must be able to accept different functional behaviour.
    2. The "client" must be able to queue behaviour.
    3. The "client" must be able to undo the requested behaviour.

    This can be accomplished using functional pointers...