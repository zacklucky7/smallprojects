#ifndef CLIENT_H
#define CLIENT_H

#include "command.h"
#include <vector>

class Client {
    Command* activeCommand_ = nullptr;
    std::vector<Command*> cmdQueue;
public:
    void swap(Command* newCommand);
    void execute();
    void addQueue();
    void doQueue();
    void popQueue();
};

#endif
