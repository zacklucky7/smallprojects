#ifndef MASK_H
#define MASK_H

#include <bitset>

class Mask {
    // o1-4 refer to octets; subnet masks are a set of four octets in binary
    std::bitset<8> octs_[4];

    // the first octet that is non-zero
    int significant_oct_;

    // the final mask result, like /24
    int mask_;

    void calc_mask();

public:
    Mask(const int o1, const int o2, const int o3, const int o4);
    void printinfo();
};

#endif