# SmallProjects

## Name

Subnetter

## Description
This is a project that I tackled to help me with subnet calculation during my CCNA study.

The primary feature of this program is invocation via CLI arguments; you can run it like so:

subnetter.exe 255.255.255.192
...or another entry.