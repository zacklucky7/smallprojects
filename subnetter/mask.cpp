#include "mask.h"

Mask::Mask(
    const int o1 = 255,
    const int o2 = 255,
    const int o3 = 255,
    const int o4 = 0) {

    int octs[4] = {o1, o2, o3, o4};

    const int DIVIDERS = 3;

    bool broken = false;
    for (int i = DIVIDERS; i >= 0; i--) {
        if (!broken) {
            if (octs[i] <= 0 ) {
                octs_[i] = 0;
                // move to next octet
            } else {
                broken = true;
                significant_oct_ = i;
                if (octs[i] > 255) {
                    octs_[i] = 255;
                } else {
                    octs_[i] = octs[i];
                }
            }
        } else {
            octs_[i] = 255;   // preceding octets cannot be anything but 255
        }
    }

    calc_mask();

}

void Mask::printinfo() {

    printf("The octets stored are %d.%d.%d.%d\n", octs_[0], octs_[1], octs_[2], octs_[3]);
    printf("The subnet mask is /%d\n", mask_);

}

void Mask::calc_mask() {

    /*  
        first, we need the position of the significant octet, which is the octet that contains the first non-zero pattern.
        the constructor stored this as significant_oct_, referred to as so from here on.

        this can range from 0-3.
        32 - ((3 - so) * 8) will give us the basis of our subnet mask.
        if so is 3, then the base mask will be 32.

        we will then iterate through the significant octet to find the first non-zero digit.
    */

    static const int MAX_OCT_BITS = 32;
    static const int OCT_BITS = 8;

    int mask_base = MAX_OCT_BITS - ((3 - significant_oct_) * OCT_BITS);

    std::bitset<8>& sig_oct = octs_[significant_oct_];

    int pos = 0;
    // remember that bitset octets are right to left; 0 is rightmost, 7 is leftmost
    for (int test_bit = 0; test_bit < OCT_BITS; test_bit++) {
        if (sig_oct.test(test_bit)) {
            // it's 1, so store and terminate this
            pos = test_bit;
            break;
        }
    }

    mask_ = mask_base - pos;

}