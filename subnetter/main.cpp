#include <bitset>
#include <cstdio>
#include <cstring>

#include "mask.h"

const int MAX_MASK_LENGTH = 15;
const int DIVIDERS = 3;
const int OCTETS = 4;
const int BITS_PER_OCTET = 3;

int is_subnet(const char* to_examine) {
    // First, examine the C-string to make sure it conforms to an IPv4 address.
    
    int return_code = 0;    // 0 is failure, 1 is success

    static const int MIN_LENGTH = 3;    // ...

    size_t ex_len = strlen(to_examine);
    
    if (ex_len >= MIN_LENGTH && ex_len <= MAX_MASK_LENGTH) {
        // We have a proper length, so count if we have dividers
        int divider_count = 0;
        const char* result = to_examine;
        while ((result = strchr(result, '.')) != nullptr) {
            result++;
            divider_count++;
        }
        if (divider_count == 3) {
            return_code = 1;
        }
    }

    return return_code;

}

int read_octet(int start_index, int end_divider, const char* str_to_read) {
    char seq[BITS_PER_OCTET+1] = {};
    int seq_piece = 0;
    while (start_index < end_divider) {
        seq[seq_piece++] = str_to_read[start_index++];
    }
    int nums = atoi(seq);
    return nums;
}

Mask* make_mask(const char* to_process) {

    size_t len = strlen(to_process);
    int i_locs[DIVIDERS] = {0,0,0};

    // First, find the divider indices

    int loc_num = 0;
    for (int i = 0; i < len; i++) {
        if (to_process[i] == '.') {
            i_locs[loc_num++] = i;
        }
    }

    int octets[OCTETS] = {-1,-1,-1,-1};
    

    for (int i = 0; i <= DIVIDERS; i++) {
        if (i_locs[i] == 0) {
            // The first divider is at index 0.
            // This means the first octet is 000.
            octets[i] = 0;
        } else {
            // The divider we are working with is not at index 0.
            // Which divider are we?
            if (i == 0) {
                // We're the first divider. Grab everything since the start of the string.
                octets[i] = read_octet(0, i_locs[i], to_process);
            } else {
                // First, we are the final octet?
                if (i == DIVIDERS) {
                    // Yes. Get the rest of the characters and place them in the final octet.
                    // First, is there anything following us?
                    if (i_locs[i-1] == len-1) {
                        // No, the final octet is 0.
                        octets[i] = 0;
                    } else {
                        // Yes, get the rest.
                        octets[i] = read_octet(i_locs[i-1]+1, len, to_process);
                    }
                } else {
                    // We are not the first divider. Where is the one before us?
                    const int prev_divider = i_locs[i-1];
                    // Is that divider immediately before us?
                    if (i_locs[i] - 1 == i_locs[i-1]) {
                     // Yes, so this octet is 0
                        octets[i] = 0;
                    } else {
                        // No, so grab all characters between here and the previous divider
                        const int start_index = prev_divider + 1;
                        octets[i] = read_octet(start_index, i_locs[i], to_process);
                    }
                }                
            }
        }
    }


    // Now that the octets array is populated, let's transform it into a mask!


    Mask* sn = new Mask(octets[0], octets[1], octets[2], octets[3]);
    
    return sn;
}

int main(int argc, char* argv[]) {

    Mask* sn = nullptr;

    if (argc == 2) {
        const char* arg = argv[1];
        if (is_subnet(arg)) {
            sn = make_mask(arg);
        } else {
            printf("Given argument is not a subnet mask\n");
        }
    } else if (argc > 2) {
        printf("Too many arguments.\n");
    } else {
        printf("Provide an IPv4 subnet in a four-octet format (XXX.XXX.XXX.XXX)\n");
    }

    if (sn) {
        sn->printinfo();
        // Cleanup
        delete sn;
        sn = nullptr;
    } else {
        printf("Failed to create subnet mask.\n");
    }

    return 0;

}