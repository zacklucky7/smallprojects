# SmallProjects

## Description
This is a small selection from the sprawling number of small C++ projects that I had an interest in putting together.

Each folder is its own project with its own readme file describing its purpose.
