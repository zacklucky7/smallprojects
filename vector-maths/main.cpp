#include <iostream>

#include "LAVector.h"

int main() {

    LAVector lav1 {5, 0};
    LAVector lav2 {2, 3};
    LAVector lav3 = lav1.vector_proj(lav2);

    std::cout << "lav1: " << lav1 << std::endl;
    std::cout << "lav2: " << lav2 << std::endl;
    std::cout << "dot project: " << lav1.dot(lav2);

    return 0;
}