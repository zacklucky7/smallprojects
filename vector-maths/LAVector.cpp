#include <iostream>
#include <sstream>

#include "LAVector.h"

LAVector::LAVector(std::initializer_list<double> list) {

    for (double item: list) {
        elements_.push_back(item);
    }

}

LAVector::LAVector(const std::vector<double>& vec) {

    for (double e: vec) {
        elements_.push_back(e);
    }

}

void LAVector::read() const {
    for (double e: elements_) {
        std::cout << "e: " << e << std::endl;
    }
}

const double LAVector::at(const int index) const {

    if (elements_.size() > 0) {
        return elements_.at(index);
    } else {
        return 0;
    }

}

const std::string LAVector::info() const {

    std::ostringstream os;
    os << "(";
    for (size_t i = 0; i < size(); i++) {
        if (i + 1 != size()) {  // If this isn't the last element
            os << at(i) << ", ";
        } else {
            os << at(i);
        }
    }
    os << ")";

    return os.str();

}

std::ostream& operator<< (std::ostream& os, const LAVector& lav) {
    os << lav.info();
    return os;
}

LAVector LAVector::operator+ (const LAVector& other_vec) {

    std::vector<double> new_elements {};
    if (size() == other_vec.size()) {
        for (size_t i = 0; i < size(); i++) {
            new_elements.push_back(at(i) + other_vec.at(i));
        }
    }

    return LAVector(new_elements);

}

LAVector LAVector::operator- (const LAVector& other_vec) {

    std::vector<double> new_elements {};
    if (size() == other_vec.size()) {
        for (size_t i = 0; i < size(); i++) {
            new_elements.push_back(at(i) - other_vec.at(i));
        }
    }

    return LAVector(new_elements);

}

LAVector LAVector::operator* (const LAVector& other_vec) {

    std::vector<double> new_elements {};
    if (size() == other_vec.size()) {
        for (size_t i = 0; i < size(); i++) {
            new_elements.push_back(at(i) * other_vec.at(i));
        }
    }

    return LAVector(new_elements);

}

LAVector LAVector::operator* (const int scalar) {
    
    std::vector<double> new_elements {};
    if (size() > 0) {
        for (size_t i = 0; i < size(); i++) {
            new_elements.push_back(at(i) * scalar);
        }
    }

    return LAVector(new_elements);

}

void LAVector::scalar(const int scalar) {

    if (size() > 0) {
        for (size_t i = 0; i < size(); i++) {
            elements_.at(i) *= scalar;
        }
    }

}

const double LAVector::dot(const LAVector& other_vec) const {

    double dot_p = 0;
    
    if (size() == other_vec.size()) {
        for (int i = 0; i < size(); i++) {
            dot_p += (at(i) * other_vec.at(i));
        }
    }

    return dot_p;

}

const double LAVector::scalar_proj(const LAVector& project_onto) {
    // Get the projection factor of this vector onto the project_onto vector.

    if(size() == project_onto.size()) {
        return dot(project_onto) / project_onto.dot(project_onto);
    } else {
        return -1;
    }

}

LAVector LAVector::vector_proj(const LAVector& project_onto) {

    std::vector<double> new_elements {};

    if (size() == project_onto.size()) {
        const double s_proj = scalar_proj(project_onto);
        for (size_t i = 0; i < size(); i++) {
            new_elements.push_back(project_onto.at(i) * s_proj);
        }
    }

    return LAVector(new_elements);

}