# SmallProjects

## Name

Vector Maths

## Description
This is a small project that I was interested in writing while I learned bits of linear algebra.

It has some linear algebraic math functions; not an entire suite, but some as I move along with the learning.

The primary interesting component of this project is the use of initializer list to allow for a variable vector dimensionality.