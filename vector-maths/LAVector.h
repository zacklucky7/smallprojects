#ifndef LAVECTOR_H
#define LAVECTOR_H

#include <initializer_list>
#include <string>
#include <vector>

class LAVector {

    std::vector<double> elements_;

public:

    LAVector(std::initializer_list<double> list);
    LAVector(const std::vector<double>& vec);
    const double at(const int i) const;

    const std::string info() const;
    void read() const;
    const int size() const {return elements_.size();}

    friend std::ostream& operator<< (std::ostream& os, const LAVector& lav);
    LAVector operator+ (const LAVector& other_vec);
    LAVector operator- (const LAVector& other_vec);
    LAVector operator* (const LAVector& other_vec);
    LAVector operator* (const int scalar);

    void scalar(const int scalar);
    const double dot(const LAVector& other_vec) const;
    const double scalar_proj(const LAVector& project_onto);
    LAVector vector_proj(const LAVector& project_onto);

};

#endif